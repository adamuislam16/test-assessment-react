import React, { createContext, useState } from "react"
import axios from 'axios'
import { useNavigate } from "react-router-dom"
import Cookies from 'js-cookie'

export const GlobalContext = createContext()

export const GlobalProvider = (props) => {

    let navigate = useNavigate()
    
    //materi fetch data
    const [data, setData] = useState(null)

    //materi create data
    const [input, setInput] = useState(
        {
            name: "",
            address: "",
            gender: "",
            born_date: ""
        }
    )

    //indikator
    const [fetchStatus, setFetchStatus] = useState(true)

    //indikator
    const [currentId, setCurrentId] = useState(-1)

    let state = {
        data,
        setData,
        input, 
        setInput,
        fetchStatus,
        setFetchStatus,
        currentId,
        setCurrentId
    }

    //handling input
    const handleInput = (event) => {
        let name = event.target.name
        let value = event.target.value

        setInput({ ...input, [name]: value }) 
    }

    //handling submit
    const handleSubmit = (event) => {
        event.preventDefault()
        console.log(input)

        let {
            name, 
            address,
            gender,
            born_date
        } = input

        if (currentId === -1) {
            //create data
            axios.post('https://cms-admin-v2.ihsansolusi.co.id/testapi/user', 
            {
                name, 
                address,
                gender,
                born_date
            },
            {headers: {"Authorization" : "Bearer "+ Cookies.get('token')}})
            .then((res) => {
                console.log(res)
                setFetchStatus(true)
                navigate('/tabel')
            })
        } else {
            // update data
            axios.put(`https://cms-admin-v2.ihsansolusi.co.id/testapi/user/${currentId}`, 
            {
                name, 
                address, 
                gender,
                born_date
            },
            {headers: {"Authorization" : "Bearer "+ Cookies.get('token')}})
            .then((res) => {
                console.log(res.data)
                setFetchStatus(true)
                navigate('/tabel')
            })
        }
    
        //balikin indikator ke -1
        setCurrentId(-1)

        //clear input setelah create data
        setInput(
            {
                name: "",
                address: "",
                gender: "",
                born_date: ""
            }
        )
    }

    //handling edit
    const handleEdit = (event) => {
        let idData = parseInt(event.currentTarget.value)
    
        setCurrentId(idData)
        navigate(`tabel/Edit/${idData}`)
    }

    let handleFunction = {
        handleInput,
        handleSubmit,
        handleEdit,
    }

    return(
        <GlobalContext.Provider value={
            {state, handleFunction}
        }>
            {props.children}
        </GlobalContext.Provider>
    )
}