import React, { useContext, useEffect } from "react";
import { Link, useParams } from "react-router-dom";
import axios from "axios";
import { GlobalContext } from '../context/GlobalContext'
import Cookies from "js-cookie";

const Edit = () => {
    let {idData} = useParams()
    const {state, handleFunction} = useContext(GlobalContext)
    const {input, setInput} = state
    const {handleInput, handleSubmit} = handleFunction

    useEffect(() => {
        if(idData !== undefined){
            axios.get(`https://cms-admin-v2.ihsansolusi.co.id/testapi/user/${idData}`,
            { headers: { "Authorization": "Bearer " + Cookies.get('token') } })
          .then((res) => {
            let data = res.data.data
            console.log(data)
    
            setInput(
              {
                name: data.name,
                address: data.address,
                gender: data.gender,
                born_date: data.born_date,
              }
            )
          })
        }
    }, [idData, setInput])
    
    return(
        <form onSubmit={handleSubmit} className="tambah">
        <table>
        <thead>
                    <tr>
                        <td>Nama:</td>
                        <td><input type="text" name="name" id="name" onChange={handleInput} value={input.name} required /></td>
                    </tr>               
                    <tr>
                        <td>Alamat:</td>
                        <td><input type="text" name="address" id="address" onChange={handleInput} value={input.address} /></td>
                    </tr>             
                    <tr>
                        <td>P/W:</td>
                        <td><input type="text" name="gender" id="gender" onChange={handleInput} value={input.gender} placeholder="  l/p"/></td>
                    </tr>
                    <tr>
                        <td>Tanggal Lahir:</td>
                        <td><input type="date" name="born_date" id="born_date" onChange={handleInput} value={input.born_date}/></td>
                    </tr>
                </thead>
        </table>
        <div>
            <button type="submit" className="update">Update</button>
            <Link to={'/tabel'}>
            <button type="submit" className="back">Back</button>
            </Link>
        </div>
    </form>
    )
}

export default Edit