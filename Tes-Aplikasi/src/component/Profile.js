import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import axios from "axios";
import Cookies from "js-cookie";

const Profile = () => {
    const [data, setData] = useState(null)
    let { id } = useParams()
    // console.log(id)
    useEffect(() => {
        if (id !== undefined) {
            axios.get(`https://cms-admin-v2.ihsansolusi.co.id/testapi/user/${id}`,
                { headers: { "Authorization": "Bearer " + Cookies.get('token') } })
                .then((res) => {
                    console.log(res.data.data)
                    setData(res.data.data)
                })
        }
    }, [id])
    console.log(data)

    return (
        <>
            <h1 className="judul">Detail User</h1>
            <table className="tabel">
                <thead>
                    <tr>
                        <th>Username</th>
                        <th>Nama</th>
                        <th>Alamat</th>
                        <th>P/W</th>
                        <th>Tanggal Lahir</th>
                        <th>Tanggal Input</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{data?.user_name}</td>
                        <td>{data?.name}</td>
                        <td>{data?.address}</td>
                        <td>{data?.gender}</td>
                        <td>{data?.born_date}</td>
                        <td>{data?.created_at}</td>
                    </tr>
                </tbody>
            </table>
            <Link to={'/tabel'}>
                <button type="submit" className="kembali">Back</button>
            </Link>
        </>
    )
}

export default Profile