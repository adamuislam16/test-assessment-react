import React, { useState } from "react";
import "../css/App.css"
import axios from "axios";
import { Link, useNavigate } from "react-router-dom";

const Regis = () => {
    const navigate = useNavigate()

    const [input, setInput] = useState(
        {
            name: "",
            email: "",
            password: "",
        }
    )

    //handling input
    const handleInput = (event) => {
        let name = event.target.name
        let value = event.target.value

        setInput({ ...input, [name]: value })
    }
    const handleRegister = (event) => {
        event.preventDefault()
        
        let {
            name,
            email,
            password,
        } = input

        axios.post('https://cms-admin-v2.ihsansolusi.co.id/testapi/auth/register', {
            name,
            email,
            password,
        })
            .then((res) => {
                let { data } = res
                console.log(data)
                navigate('/')
            })
            .catch((error) => {
                alert(error)
            })
        setInput(
            {
                name: "",
                email: "",
                password: "",
            }
        )
    }

    return (
        <form onSubmit={handleRegister} className="Regis">
            <h1 className="Judul">Register</h1>
            <table className="daftar">
                <thead>
                    <tr>
                        <td className="Email">Name</td>
                        <td><input onChange={handleInput} value={input.name} type="text" name="name" id="name" required /></td>
                    </tr>
                    <tr>
                        <td className="Email">Email</td>
                        <td><input onChange={handleInput} value={input.email} type="email" name="email" id="email" placeholder="Name@gmail.com" required /></td>
                    </tr>
                    <tr>
                        <td className="pass">Password</td>
                        <td><input onChange={handleInput} value={input.password} type="password" name="password" id="password" placeholder="********" required /></td>
                    </tr>
                </thead>
            </table>
            <div className="tombol">
                <button type="submit" className="but">Register</button>
            </div>
            <div className="tombol">
            Already have an account? <Link to="/" className="text-black-700 font-bold hover:underline dark:text-black">Sign In</Link>
            </div>
        </form>
    )
}

export default Regis