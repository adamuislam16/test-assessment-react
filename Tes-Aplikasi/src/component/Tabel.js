import React, { useContext, useEffect } from "react";
import "../css/App.css"
import axios from "axios";
import { Link, useNavigate } from "react-router-dom";
import Cookies from "js-cookie";
import { GlobalContext } from '../context/GlobalContext'

const Tabel = () => {
    const {state, handleFunction} = useContext(GlobalContext)
    let {data, setData, fetchStatus, setFetchStatus} = state
    let { handleEdit} = handleFunction
    
    const navigate = useNavigate()

    useEffect(() => {
        if (fetchStatus === true){
            axios.get("https://cms-admin-v2.ihsansolusi.co.id/testapi/user",
                { headers: { "Authorization": "Bearer " + Cookies.get('token') } })
                .then((res) => {
                    setData(res.data.data)
                })
                .catch((error) => {
                })
        setFetchStatus(false)
            }
        }, [fetchStatus, setFetchStatus, setData])

    const handleDelete = (event) => {
        let id = parseInt(event.target.value)

        axios.delete(`https://cms-admin-v2.ihsansolusi.co.id/testapi/user/${id}`,
            { headers: { "Authorization": "Bearer " + Cookies.get('token') } })
            .then((res) => {
                setFetchStatus(true)
            })
    }

    const handleLogout = () => {
        Cookies.remove('token')
        .then((res) => {
            setFetchStatus(true)
        })
        navigate('/')
    }
    console.log(data)

    return (
        <>
            <Link to={'/tabel/tambah'}>
                <button className="Tambah">Tambah User</button>
            </Link>
            {
                !Cookies.get('token') &&
                <Link to={'/'}>
                    <button className="Login">Login</button>
                </Link>
            }
            {
                Cookies.get('token') &&
                <Link onClick={handleLogout}>
                    <button className="Login">Logout</button>
                </Link>
            }
            <table className="tabel">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Alamat</th>
                        <th>P/W</th>
                        <th>Tanggal Lahir</th>
                        <th>Tanggal Input</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                {data !== null && data.map((res, index) => {
                    return (
                        <tbody key={res.id}>
                            <tr>
                                <td>{index + 1}</td>
                                <td>{res.name}</td>
                                <td>{res.address}</td>
                                <td>{res.gender}</td>
                                <td>{res.born_date}</td>
                                <td>{res.created_at}</td>
                                <td><div>
                                    <Link to={`/tabel/view/${res.id}`} key={res.id}>
                                        <button className="view">
                                            View
                                        </button>
                                    </Link>
                                        <button onClick={handleEdit} value={res.id} className="Edit">
                                            Edit
                                        </button>
                                    <button onClick={handleDelete} value={res.id}>
                                        Delete
                                    </button>
                                </div>
                                </td>
                            </tr>
                        </tbody>
                    )
                })}
            </table>
        </>
    )
}

export default Tabel