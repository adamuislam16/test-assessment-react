import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";
import Cookies from "js-cookie";

const Login = () => {
    const navigate = useNavigate()

    const [input, setInput] = useState(
        {
            email: "",
            password: ""
        }
    )

    //handling input
    const handleInput = (event) => {
        let name = event.target.name
        let value = event.target.value

        setInput({ ...input, [name]: value })
    }

    //handlingLogin
    const handleLogin = (event) => {
        event.preventDefault()

        let {
            email,
            password,
        } = input

        axios.post('https://cms-admin-v2.ihsansolusi.co.id/testapi/auth/login', { email, password })
            .then((res) => {
                let { data } = res
                console.log(data)

                let { token, user } = data

                Cookies.set('token', token, { expires: 1 })
                Cookies.set('user_data', JSON.stringify(user), { expires: 1 })
                console.log(user)
                navigate('/tabel')
            })
            .catch((error) => {
                alert(error)
            })
    }

    return (
        <form onSubmit={handleLogin} className="login">
            <h1 className="Judul">Login</h1>
            <table className="daftar">
                <thead>
                    <tr>
                        <td className="Email">Email</td>
                        <td><input value={input.email} onChange={handleInput} type="email" name="email" id="email" placeholder="Name@gmail.com" required/></td>
                    </tr>
                </thead>
                <thead>
                    <tr>
                        <td className="pass">Password</td>
                        <td><input value={input.password} onChange={handleInput} type="password" name="password" id="password" placeholder="*******" required/></td>
                    </tr>
                </thead>
            </table>
            <div className="tombol">
                <button type="submit" className="but">Login</button>
            </div>
            <Link to={'/regis'}>
                <div className="tombol">
                    <button type="submit" className="but">Create account</button>
                </div>
            </Link>
        </form>
    )
}

export default Login