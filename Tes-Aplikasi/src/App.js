import React from "react";
import { BrowserRouter, Route, Routes, Navigate } from "react-router-dom";
import { GlobalProvider } from './context/GlobalContext';
import Cookies from 'js-cookie'
import Tabel from "./component/Tabel";
import Tambah from "./component/Tambah";
import Regis from "./component/Regis";
import Profile from "./component/Profile";
import Edit from "./component/Edit";
import Login from "./component/Login";

function App() {
  const LoginRoute = (props) => {
    if (Cookies.get('token') === undefined) {
      return props.children
    } else if (Cookies.get('token') !== undefined) {
      return <Navigate to={'/'} />
    }
  }
  const TableRoute = (props) => {
    if (Cookies.get('token') !== undefined) {
      return props.children
    } else if (Cookies.get('token') === undefined) {
      return <Navigate to={'/'} />
    }
  }

  return (
    <>
      <BrowserRouter>
        <GlobalProvider>
          <Routes>

            <Route path="/" element={
              <LoginRoute>
                <Login />
              </LoginRoute>
            } />

            <Route path="/tabel/tambah" element={
              <Tambah />
            } />

            <Route path="/regis" element={
              <Regis />
            } />

            <Route path="/tabel/view/:id" element={
              <Profile />
            } />

            <Route path="/tabel/Edit/:idData" element={
              <Edit />
            } />

            <Route path="/tabel" element={
              <TableRoute>
              <Tabel />
              </TableRoute>
            } />

          </Routes>
        </GlobalProvider>
      </BrowserRouter>
    </>
  );
}

export default App;
