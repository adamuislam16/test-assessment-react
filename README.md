Cara Instalasi

1. clone terlebih dahulu file gitnya, dengan cara:
- copy text Clone gitnya 
- lalu jalankan perintah ini di terminal/cmd/git bash pada komputer/laptop anda
- > git clone hasil_copy_dari_clone_with_https
- lalu pastikan cloning folder project yang sudah ketempat yang sudah kamu rencanakan ya
2. kemudian buka folder file hasil clone ke software visual studio code
3. dan langkah terakhir jalankan kode tersebut dengan kode "npm run start"
, dan tunggu hingga prosesnya selesai.
4. Berhasil

Note:
1. pada aplikasi tersebut saya menggunakan react router dom. dimana cara instalasinya yaitu "npm install react-router-dom@6"
2. kemudian saya juga menggunakan axios, untuk instalasinya sebagai berikut "npm install axios"

Terima Kasih